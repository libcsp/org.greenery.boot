
dependencyResolutionManagement {
    versionCatalogs{
        create("libs"){
            from(files("${rootDir}/libs.versions.toml"))
        }
    }
}
rootProject.name = System.getProperty("name")
include("mixed-program")
include("Jsnxopenapi")
include("mybatis-flex-template")
include("callcenter")
include("moia")
include("daas-sms")
include("db2sync")
