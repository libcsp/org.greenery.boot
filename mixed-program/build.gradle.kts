application{
    this.mainClass.set("org.greenery.boot.JavaApplication")
}
dependencies{
    implementation(libs.bundles.spring.boot.starter.web)
    implementation(libs.scala3.library)
    implementation(libs.scala2.library)
    implementation(libs.groovy.all)
    implementation(libs.commons.io)

}