package org.greenery.boot
import scala.language.postfixOps

case class WithLog[A](value: A, log: String) {
  def map[B](f: A => B): WithLog[B] =
    WithLog(f(value), s"$log\nApplied map: $value -> ${f(value)}")

  def flatMap[B](f: A => WithLog[B]): WithLog[B] =
    WithLog(f(value).value, s"$log\n${f(value).log}")

  override def toString: String = s"Value: $value\nLog:$log"
}
@main
def main(args:String*):Unit = {
  val result = for {
    a <- WithLog(1, "Initial value: 1")
    b <- WithLog(a + 2, s"Added 2 to $a")
    c <- WithLog(b * 3, s"Multiplied $b by 3")
  } yield c
  println(result)
  val x = WithLog(1, "Initial value: 1")
    .flatMap(a => WithLog(a + 2, s"Added 2 to $a"))
    .flatMap(b => WithLog(b * 3, s"Multiplied $b by 3"))
    .map(c => WithLog(c * 3, s"Multiplied $c by 3"))
  println(x)
}
