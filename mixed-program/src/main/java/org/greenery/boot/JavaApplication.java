package org.greenery.boot;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JavaApplication {
    public static void main(String[] args) throws IOException {
        System.out.println("hello java!");
        String con = IOUtils.toString(JavaApplication.class.getResourceAsStream("/conf/config.properties"), StandardCharsets.UTF_8);
        System.out.println(con);

    }
}
