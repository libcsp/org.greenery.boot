import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("version-catalog")
    id("maven-publish")
    id("java-library")
    alias(libs.plugins.spring.boot)
//    alias(libs.plugins.javafx)
    java
    idea
//    groovy
    scala
    kotlin("jvm") version System.getProperty("kotlin.version")
    application
}
buildscript {
    repositories {
        maven(url = "https://mirrors.huaweicloud.com/repository/maven/")
    }

}
allprojects {
    repositories{
        maven(url = "https://mirrors.huaweicloud.com/repository/maven/")
    }
    group = System.getProperty("group")
    version = System.getProperty("version")

    apply(plugin = "java")
    apply(plugin = "idea")
    apply(plugin = "scala")
    apply(plugin = "kotlin")
//    apply(plugin = "groovy")
    apply(plugin = "org.springframework.boot")
    apply(plugin = "application")
    apply(plugin = "version-catalog")
    apply(plugin = "maven-publish")
    apply(plugin = "java-library")
//    apply(plugin = "org.openjfx.javafxplugin")

    catalog {
        versionCatalog {
            from(files("${rootDir}/libs.versions.toml"))
        }
    }

    application {
        mainClass.value("")
    }

    // Java
    val javaCompileArgs = listOf(
        "--enable-preview"
    )

//    idea{
//        module {
//            inheritOutputDirs = true
//        }
//    }


    java {
        sourceCompatibility = JavaVersion.toVersion(System.getProperty("jdk.version"))
        targetCompatibility = JavaVersion.toVersion(System.getProperty("jdk.version"))
        java.withSourcesJar()
        modularity.inferModulePath.set(true)
    }
//
//    javafx{
//        version = System.getProperty("jdk.version")
//        modules = listOf("javafx.controls", "javafx.graphics", "javafx.fxml", "javafx.web", "javafx.media", "javafx.base")
//    }



    //[compileJava, compileTestJava, javadoc]*.options*.encoding = 'UTF-8'
    tasks.compileJava {
        this.options.encoding = "UTF-8"
        this.options.compilerArgs = javaCompileArgs
    }
    tasks.compileTestJava {
        this.options.encoding = "UTF-8"
        this.options.compilerArgs = javaCompileArgs
    }
    tasks.javadoc {
        this.options.encoding = "UTF-8"
    }

    tasks.withType<JavaExec> {
        this.jvmArgs = listOf(
            "-Dsun.io.unicode.encoding=UTF-8",
            "-Dsun.stdout.encoding=UTF-8",
            "-Dsun.jnu.encoding=UTF-8",
            "-Dstderr.encoding=UTF-8",
            "-Dstdout.encoding=UTF-8",
            "-Dnative.encoding=UTF-8",
            "-Dfile.encoding=UTF-8",
            "-XX:+UseZGC",
            "-XX:+ZGenerational",
            "-Djava.net.preferIPv4Stack=true",
            "-Dsun.net.inetaddr.ttl=30",
            "-Dsun.net.inetaddr.negative.ttl=30",
            "-Dline.separator=\n",
            "-Duser.language=zh",
            "-Duser.region=CN",
            "-Djava.security.egd=file:/dev/./urandom",
            "-Dserver.tomcat.apr=true",
            "-Dspring.output.ansi.enabled=ALWAYS"
        )
    }

    // Kotlin
    kotlin{
        sourceSets.all {
            languageSettings {
                languageVersion = "2.0"
            }
        }
    }

    tasks.withType<KotlinCompile>{
        compilerOptions {
            this.jvmTarget.set(JvmTarget.valueOf("JVM_${System.getProperty("kotlin.jvm.version")}"))
            freeCompilerArgs = listOf(
            )
        }
    }


    // Scala
     scala {
         zincVersion.set(System.getProperty("zinc.version"))
     }
    tasks.withType<ScalaCompile> {
        this.scalaCompileOptions.encoding = "UTF-8"
        this.scalaCompileOptions.additionalParameters = listOf(
//            "-target:jvm-${System.getProperty("jdk.version")}",
            "-Yexplicit-nulls"
        )
        with(this.scalaCompileOptions.forkOptions) {
            memoryMaximumSize = "1g"
            jvmArgs = listOf(
                "-XX:MaxMetaspaceSize=512m"
            )

        }
    }
    sourceSets {

        main {
            scala {
                setSrcDirs(listOf("src/main/scala"))
            }
        }
        test {
            scala {
                setSrcDirs(listOf("src/test/scala"))
            }
        }
        main {
            java {
                setSrcDirs(listOf("src/main/java"))
            }
            resources {
                this.setSrcDirs(listOf("src/main/resources"))
            }
        }
        test {
            java {
                setSrcDirs(listOf("src/test/java"))
            }
            resources {
                this.setSrcDirs(listOf("src/main/resources"))
            }
        }
    }
    dependencies{
//        implementation(rootProject.libs.scala3.library)
//        implementation(rootProject.libs.groovy.all)
        compileOnly(rootProject.libs.lombok)
        annotationProcessor(rootProject.libs.lombok)
        testAnnotationProcessor(rootProject.libs.lombok)
        testImplementation(platform(rootProject.libs.junit.bom))
        testImplementation(rootProject.libs.junit.jupiter)
    }
    tasks.test {
        group = "application"
        useJUnitPlatform()
        jvmArgs = javaCompileArgs
    }
    tasks.bootJar{
        this.group = "application"
        val output = File("${project.projectDir.path}/build/bin")
        this.archiveFileName.set("${System.getProperty("group")}.${project.name}-${System.getProperty("version")}-all.jar")
        this.destinationDirectory.set(output)
    }
    tasks.build{
        this.group = "application"
    }
    tasks.clean{
        this.group = "application"
    }

    tasks.register("env"){
        group = "application"
        System.getProperties().list(System.out)
    }
    tasks.classes {
//        doFirst{
//            copy{
//                from("${project.projectDir.path}/build/resources/main")
//                into("${project.projectDir.path}/build/classes/java/main")
//            }
//        }

    }
}
subprojects {
    tasks.register("pack"){
        this.group = "application"
        copy{
            from(
                configurations.runtimeClasspath,
                configurations.annotationProcessor,
                configurations.testAnnotationProcessor
            )
            into("${project.projectDir.path}/build/lib")
        }
    }
    tasks.jar{
        group = "application"
        val jar = this
        this.dependsOn("pack")
        this.doFirst{
            val allJars = this.project.configurations.runtimeClasspath.map {
                it.asFileTree.files
            }.get().joinToString(separator = " ") {
                "../lib/${it.name}"
            }
            jar.manifest{
                this.attributes(mutableMapOf(
                    "Main-Class" to project.application.mainClass.get(),
                    "Class-Path" to "$allJars ."
                ))
            }
            val output = File("${project.projectDir.path}/build/bin")
            output.mkdirs()
//            val oldArchiveFileName = jar.archiveFileName.get()
//            jar.destinationDirectory.set(output)
//            jar.archiveFileName.set("${System.getProperty("group")}.${project.name}-${System.getProperty("version")}.jar")
            copy {
                from(jar.destinationDirectory)
                into("${project.projectDir.path}/build/bin")
                rename(jar.archiveFileName.get(), "${System.getProperty("group")}.${project.name}-${System.getProperty("version")}.jar")
            }
        }
    }
}
