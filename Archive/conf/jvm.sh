#!/bin/sh

# shellcheck disable=SC2034
JAVA_HOME=${HOME}/tools/jdk

# -Xms:最小堆内存 -Xmx:最大堆内存 -Xmn:年轻代大小  
# 整个堆大小=年轻代大小 + 年老代大小 + 持久代大小
# 持久代大小一般为 64MB 
# 增大年轻代后,将会减小年老代大小.此值对系统性能影响较大,Sun官方推荐配置为整个堆的3/8.
# shellcheck disable=SC2034
MEM_OPTS="-Xms8g -Xmx8g -Xmn4g"
# -XX:+HeapDumpOnOutOfMemoryError 参数表示当JVM发生OOM时，自动生成DUMP文件
# -XX:G1HeapRegionSize=16m 使用G1时Java堆会被分为大小统一的的区(region)。此参数可以指定每个heap区的大小. 默认值将根据 heap size 算出最优解. 最小值为 1Mb, 最大值为 32Mb.
# -XX:G1ReservePercent=25 设置堆内存保留为假天花板的总量,以降低提升失败的可能性. 默认值是 10.
# -XX:InitiatingHeapOccupancyPercent=30 启动并发GC周期时的堆内存占用百分比. G1之类的垃圾收集器用它来触发并发GC周期,基于整个堆的使用率,而不只是某一代内存的使用比. 值为 0 则表示"一直执行GC循环". 默认值为 45.
# -XX:SoftRefLRUPolicyMSPerMB=1000 在有大量反射代码的场景下使用
JAVA_OPTS="$JAVA_OPTS -XX:+HeapDumpOnOutOfMemoryError -XX:+UseG1GC -XX:G1HeapRegionSize=16m -XX:G1ReservePercent=25"
JAVA_OPTS="$JAVA_OPTS -XX:InitiatingHeapOccupancyPercent=30 -XX:SoftRefLRUPolicyMSPerMB=0"
# -Djava.net.preferIPv4Stack=true 只支持ipv4
# -Dsun.net.inetaddr.ttl JVM DNS IP地址缓存 JVM DNS IP地址缓存 在主机名解析为 IP 地址后，资源 IP 地址将保存在 JVM 的高速缓存中。如果改变了资源的 IP 地址，则需要重新启动应用服务器，使 Identity Manager 能够检测所做更改 (ID-3635)。这是 Sun JDK（1.3 及更高版本）中的设置，可以使用 sun.net.inetaddr.ttl 属性（通常在 jre/lib/security/java.security 中设置）控制 设置解析成功的域名记录JVM中缓存的有效时间，JVM默认是永远有效，这样一来域名IP重定向必须重启JVM，这里修改为5秒钟有效，0表示禁止缓存，-1表示永远有效
# -Dsun.net.inetaddr.negative.ttl=30  设置解析失败的域名记录JVM中缓存的有效时间，JVM默认是10秒，0表示禁止缓存，-1表示永远有效
JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true -Dsun.net.inetaddr.ttl=30 -Dsun.net.inetaddr.negative.ttl=30"
# -Dfile.encoding 强行设置系统文件编码格式为utf-8.
# -Djava.security.egd ecureRandom在java各种组件中使用广泛，可以可靠的产生随机数。但在大量产生随机数的场景下，性能会较低。这时可以使用"-Djava.security.egd=file:/dev/./urandom"加快随机数产生过程。
JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8 -Duser.language=zh -Duser.region=CN -Djava.security.egd=file:/dev/zero"
# Spring启动端口
JAVA_OPTS="$JAVA_OPTS -Dserver.port=10301"
# Spring 启动激活配置文件
JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=test2"
#nohup $JAVA_HOME/bin/java -server $MEM_OPTS $JAVA_OPTS -jar ./server-v2.1.jar > log.log 2>&1 &

