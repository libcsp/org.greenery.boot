package org.greenery.boot.conf;

import java.util.List;

public class JavaOptions {
    private List<String> jvmArgs;

    public List<String> getJvmArgs() {
        return jvmArgs;
    }

    public void setJvmArgs(List<String> jvmArgs) {
        this.jvmArgs = jvmArgs;
    }
}
