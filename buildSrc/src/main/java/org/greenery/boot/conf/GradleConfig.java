package org.greenery.boot.conf;

import org.greenery.boot.conf.entity.NexusRepository;
import org.greenery.boot.conf.entity.PublicRepository;

import java.util.List;

public class GradleConfig {
    private List<PublicRepository> publicRepositories;
    private List<NexusRepository> nexusRepositories;

    private List<String> javac;

    private List<String> scalac;

    private List<String> kotlinc;

    public List<PublicRepository> getPublicRepositories() {
        return publicRepositories;
    }

    public void setPublicRepositories(List<PublicRepository> publicRepositories) {
        this.publicRepositories = publicRepositories;
    }

    public List<NexusRepository> getNexusRepositories() {
        return nexusRepositories;
    }

    public void setNexusRepositories(List<NexusRepository> nexusRepositories) {
        this.nexusRepositories = nexusRepositories;
    }

    public List<String> getJavac() {
        return javac;
    }

    public void setJavac(List<String> javac) {
        this.javac = javac;
    }

    public List<String> getScalac() {
        return scalac;
    }

    public void setScalac(List<String> scalac) {
        this.scalac = scalac;
    }

    public List<String> getKotlinc() {
        return kotlinc;
    }

    public void setKotlinc(List<String> kotlinc) {
        this.kotlinc = kotlinc;
    }
}
