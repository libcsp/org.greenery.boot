package org.greenery.boot.conf.entity;

import java.util.List;

public class NexusRepository {
    private Boolean enable;
    private Boolean allowPublish;
    private String url;
    private List<String> groups;
    private Boolean allowInsecureProtocol;
    private String username;
    private String password;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Boolean getAllowPublish() {
        return allowPublish;
    }

    public void setAllowPublish(Boolean allowPublish) {
        this.allowPublish = allowPublish;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public Boolean getAllowInsecureProtocol() {
        return allowInsecureProtocol;
    }

    public void setAllowInsecureProtocol(Boolean allowInsecureProtocol) {
        this.allowInsecureProtocol = allowInsecureProtocol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
