package org.greenery.boot.conf.entity;

public class PublicRepository {
    private Boolean enable;
    private String url;
    private Boolean allowInsecureProtocol;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getAllowInsecureProtocol() {
        return allowInsecureProtocol;
    }

    public void setAllowInsecureProtocol(Boolean allowInsecureProtocol) {
        this.allowInsecureProtocol = allowInsecureProtocol;
    }
}
