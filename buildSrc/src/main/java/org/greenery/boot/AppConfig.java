package org.greenery.boot;

import com.google.gson.Gson;
import org.codehaus.groovy.util.StringUtil;
import org.gradle.api.Project;
import org.greenery.boot.conf.GradleConfig;
import org.greenery.boot.conf.JavaOptions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class AppConfig {
    public final GradleConfig GRADLE_CONF;
    public final JavaOptions JAVA_OPTIONS;

    public static final String ENV = System.getProperty("env");

    public AppConfig(Project project){
        String rootDir = project.getRootDir().getAbsolutePath();
        try {
            GRADLE_CONF = new Gson().fromJson(Files.readString(new File("%s/gradle.conf.json".formatted(rootDir)).toPath()), GradleConfig.class);
            JAVA_OPTIONS = new Gson().fromJson(Files.readString(new File("%s/env-%s.json".formatted(rootDir,ENV.toLowerCase())).toPath()), JavaOptions.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
