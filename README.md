# Gradle入门

#### 介绍
写好的Gradle脚本，开箱即用，让配置简单些，默认配置好了四种JVM语言，
Scala,Java,Groovy,Kotlin。

#### 软件架构
- Gradle 8.12.1
- Java 21
- Kotlin 2.0.0
- Groovy 4.0.25
- Scala 3.6.3
- Zinc 1.10.7

#### 安装教程

1.  打开gradle.properties文件，修改JDK_VERSION版本为你电脑上的对应JDK版本，
2. 剩下开箱即用,无需任何更改

#### 使用说明

1.  gradle.properties文件里更改插件下载的源
2.  gradle.conf.json可以更改依赖下载的源和编译选项
3.  env-dev.json env-prod.json取决于gradle.properties中的env变量,用于java运行时参数.

#### 参与贡献

1.  zhu bin
2. QQ 582521582


#### 特技

1.  已经配置好4种JVM语言