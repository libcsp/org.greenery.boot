module jfx.core{


    requires java.base;
    requires java.logging;
    requires jdk.net;

    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.base;
    requires javafx.graphics;
    requires javafx.media;
    requires javafx.swing;


    requires lombok;


    opens org.greenery.boot;

    exports  org.greenery.boot;


}